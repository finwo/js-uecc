function MiniPromise(cb) {
  var self       = this;
  this.__next    = null;
  this.__started = false;
  this.__catch   = undefined;
  this.start     = function (data) {
    this.__started = true;
    var result     = undefined;
    if ( 'function' !== typeof cb ) {
      return console.error('Given callback was not a function:', cb);
    }
    if (data === undefined) {
      result = cb(this.__next && this.__next.start || function () {}, this.__catch || console.error);
    } else {
      try {
        result = cb(data);
      } catch(e) {
        return (this.__catch || console.error)(e);
      }
    }
    if (result instanceof MiniPromise) {
      result.then(this.__next && this.__next.start || function () {});
      if(!result.__started) result.start();
    } else if (result !== undefined) {
      this.__next && this.__next.start && this.__next.start(result);
    }
  };
  this.then      = function (onResolve,onReject) {
    this.__next           = new MiniPromise(onResolve);
    this.__next.__catch   = onReject;
    this.__next.__started = true;
    this.then             = this.then.bind(this.__next);
    this.catch            = this.catch.bind(this.__next);
    return this;
  };
  this.catch = function (onReject) {
    this.__catch = onReject;
    return this;
  };
  setTimeout(function () {
    if (!self.__started) self.start();
  }, 0);
}

MiniPromise.all = function(arr) {
  var q = new MiniPromise(function(resolve,reject) {
    resolve();
  });
  arr.forEach(function(entry) {
    q.then(entry);
  });
  return q;
};

module.exports = MiniPromise;

